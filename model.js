
function nhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam) {
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.gioLam = gioLam;
    this.chucVu = chucVu;
    this.tinhTongLuong = function () {
        var x;
        if (this.chucVu == "2") {
            x = 3
        } else if (this.chucVu == "3") {
            x = 2;
        } else {
            x = 1
        }
        var tongLuong = this.luongCoBan * 1 * x
        return tongLuong
    }
    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return "xuất sắc";
        } else if (this.gioLam >= 176) {
            return "giỏi";
        } else if (this.gioLam >= 160) {
            return "khá";
        } else {
            return "trung bình";
        }
    }
}