var dsnv = []

function luuLocal() {
    var luuDS = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", luuDS)
}
var layDS = localStorage.getItem("DSNV")
if (layDS !== null) {
    var dsTuLocal = JSON.parse(layDS)
    for (var index = 0; index < dsTuLocal.length; index++) {
        var item = dsTuLocal[index];
        var nv = new nhanVien(
            item.taiKhoan,
            item.hoTen,
            item.email,
            item.matKhau,
            item.ngayLam,
            item.luongCoBan,
            item.chucVu,
            item.gioLam
        )
        dsnv.push(nv);
    }
    renderDsnv(dsnv)
}

function themNguoiDung() {
    var nv = layThongTin();
    var isValid = true
    // validate tài khoản
    isValid = kiemTraTrong(nv.taiKhoan, "tbTKNV", "Tài khoản không được để trống") && kiemTraTrung(nv.taiKhoan, dsnv) && kiemTraDoDai(nv.taiKhoan, "tbTKNV", "Tài khoản phải từ 4-6 kí tự");
    // validate họ tên
    isValid = isValid & kiemTraTrong(nv.hoTen, "tbTen", "Họ tên không được để trống") && kiemTraChu(nv.hoTen, "tbTen", "Tên nhân viên phải là chữ");
    // validate email
    isValid = isValid & kiemTraTrong(nv.email, "tbEmail", "Email không được để trống") && kiemTraEmail(nv.email, "tbEmail", "Email phải đúng định dạng");
    // validate mật khẩu
    isValid = isValid & kiemTraTrong(nv.matKhau, "tbMatKhau", "Mật khẩu không được để trống") && kiemTraPass(nv.matKhau, "tbMatKhau", "Mật khẩu phải từ 6-10 ký tự(ít nhất 1 ký tự số,1 ký in hoa, 1 ký tự đặc biệt)");
    // validate ngày làm
    isValid = isValid & kiemTraTrong(nv.ngayLam, "tbNgay", "Ngày làm không được để trống") && kiemTraNgayThangNam(nv.ngayLam, "tbNgay", "Ngày làm phải đúng định dạng MM/DD/YYYY");
    // validate lương cơ bản
    isValid = isValid & kiemTraTrong(nv.luongCoBan, "tbLuongCB", "Lương cơ bản không được để trống") && kiemTraLuong(nv.luongCoBan, "tbLuongCB", "Luong phải từ 1000000-20000000")
    // validate chức vụ
    isValid = isValid & kiemTraChucVu(nv.chucVu, "tbChucVu", "Vui lòng chọn chức vụ")
    // validate sô giờ làm
    isValid = isValid & kiemTraTrong(nv.gioLam, "tbGiolam", "Giờ làm không được để trống") && kiemTraGioLam(nv.gioLam, "tbGiolam", "Giờ làm phải từ 80-200 giờ")
    if (isValid) {
        dsnv.push(nv);
        luuLocal()
        renderDsnv(dsnv);
        document.getElementById("btnDong").click();
    }
}
// delete
function xoaNhanVien(taiKhoan) {
    var index = timKiemViTri(taiKhoan, dsnv)
    if (index !== -1) {
        dsnv.splice(index, 1)
        luuLocal()
        renderDsnv(dsnv)
    }
}
// edit
function suaNhanVien(taiKhoan) {
    var index = timKiemViTri(taiKhoan, dsnv)
    if (index == -1) return;
    var nv = dsnv[index] // object
    hienThiThongTin(nv)
    document.getElementById("tknv").disabled = true;
    document.getElementById("btnCapNhat").style.display = "block";
    document.getElementById("btnThemNV").style.display = "none";

}
function capNhatNhanVien() {
    //lấy object từ form đã sửa
    var nv = layThongTin()
    var index = timKiemViTri(nv.taiKhoan, dsnv)
    var isValid = true
    // validate họ tên
    isValid = isValid & kiemTraTrong(nv.hoTen, "tbTen", "Họ tên không được để trống") && kiemTraChu(nv.hoTen, "tbTen", "Tên nhân viên phải là chữ");
    // validate email
    isValid = isValid & kiemTraTrong(nv.email, "tbEmail", "Email không được để trống") && kiemTraEmail(nv.email, "tbEmail", "Email phải đúng định dạng");
    // validate mật khẩu
    isValid = isValid & kiemTraTrong(nv.matKhau, "tbMatKhau", "Mật khẩu không được để trống") && kiemTraPass(nv.matKhau, "tbMatKhau", "Mật khẩu phải từ 6-10 ký tự(ít nhất 1 ký tự số,1 ký in hoa, 1 ký tự đặc biệt)");
    // validate ngày làm
    isValid = isValid & kiemTraTrong(nv.ngayLam, "tbNgay", "Ngày làm không được để trống") && kiemTraNgayThangNam(nv.ngayLam, "tbNgay", "Ngày làm phải đúng định dạng MM/DD/YYYY");
    // validate lương cơ bản
    isValid = isValid & kiemTraTrong(nv.luongCoBan, "tbLuongCB", "Lương cơ bản không được để trống") && kiemTraLuong(nv.luongCoBan, "tbLuongCB", "Luong phải từ 1000000-20000000")
    // validate chức vụ
    isValid = isValid & kiemTraChucVu(nv.chucVu, "tbChucVu", "Vui lòng chọn chức vụ")
    // validate sô giờ làm
    isValid = isValid & kiemTraTrong(nv.gioLam, "tbGiolam", "Giờ làm không được để trống") && kiemTraGioLam(nv.gioLam, "tbGiolam", "Giờ làm phải từ 80-200 giờ")

    if (index == -1) return;
    //update nhân viên vị tri index bằng form đã sửa

    if (isValid) {
        dsnv[index] = nv;
        luuLocal()
        renderDsnv(dsnv)
        document.getElementById("btnDong").click();
    }

}
//search
function filter() {
    var filterXepLoai = document.getElementById("filterXepLoai").value;
    var cloneDsnv = dsnv;

    if (filterXepLoai == 1) {
        cloneDsnv = dsnv;
    } else if (filterXepLoai == 2) {
        cloneDsnv = dsnv.filter(nv => nv.xepLoai() === "xuất sắc")
    } else if (filterXepLoai == 3) {
        cloneDsnv = dsnv.filter(nv => nv.xepLoai() === "giỏi")
    } else if (filterXepLoai == 4) {
        cloneDsnv = dsnv.filter(nv => nv.xepLoai() === "khá")
    } else if (filterXepLoai == 5) {
        cloneDsnv = dsnv.filter(nv => nv.xepLoai() === "trung bình")
    }

    renderDsnv(cloneDsnv);
}

function moPopupThemNhanVien() {
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnCapNhat").style.display = "none";
    document.getElementById("btnThemNV").style.display = "block";
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("chucvu").value = "1";
    document.getElementById("gioLam").value = "";
}