
function layThongTin() {
    var taiKhoan = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value;
    var chucVu = document.getElementById("chucvu").value;

    var gioLam = document.getElementById("gioLam").value;
    var nv = new nhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam);
    return nv;
}



function renderDsnv(dsnv) {
    var contentHTML = "";
    for (var index = 0; index < dsnv.length; index++) {
        var currentNV = dsnv[index]

        var chucVuString = '';
        if (currentNV.chucVu == 3) {
            chucVuString = 'Trưởng phòng'
        } else if (currentNV.chucVu == 2) {
            chucVuString = 'Sếp';
        } else {
            chucVuString = 'Nhân viên'
        }

        var contentTr =
            `
        <tr>
        <td>${currentNV.taiKhoan}</td>
        <td>${currentNV.hoTen}</td>
        <td>${currentNV.email}</td>
        <td>${currentNV.ngayLam}</td>
        <td>${chucVuString}</td>  
        <td>${currentNV.tinhTongLuong()}</td>  
        <td>${currentNV.xepLoai()}</td>  
        <td class="d-flex">
        <button class="btn btn-info mr-1"
            data-toggle="modal"
            data-target="#myModal" 
            onclick="suaNhanVien('${currentNV.taiKhoan}')">Edit
        </button>
        <button class="btn btn-danger" onclick="xoaNhanVien('${currentNV.taiKhoan}')">Delete</button>
        </td>
        </tr>
        `
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML
}


function timKiemViTri(taiKhoan, dsnv) {
    for (var index = 0; index < dsnv.length; index++) {
        var nv = dsnv[index]
        if (nv.taiKhoan == taiKhoan) {
            return index;
        }
    }
    return -1;
}

function hienThiThongTin(nv) {
    document.getElementById("tknv").value = nv.taiKhoan
    document.getElementById("name").value = nv.hoTen
    document.getElementById("email").value = nv.email
    document.getElementById("password").value = nv.matKhau
    document.getElementById("datepicker").value = nv.ngayLam
    document.getElementById("luongCB").value = nv.luongCoBan
    document.getElementById("chucvu").value = nv.chucVu
    document.getElementById("gioLam").value = nv.gioLam
}
function showMessage(htmlTagId, message) {
    document.getElementById(htmlTagId).innerHTML = message
}


