
function kiemTraTrong(value, htmlTagId, message) {
    if (value.length == 0) {
        showMessage(htmlTagId, message)
        return false;
    } else {
        showMessage(htmlTagId, "")
        return true;
    }
}
function kiemTraTrung(taiKhoan, dsnv) {
    var index = timKiemViTri(taiKhoan, dsnv);
    if (index !== -1) {
        showMessage("tbTKNV", "Tài khoản không được trùng")
        return false;
    } else {
        showMessage("tbTKNV", "")
        return true;
    }
}
function kiemTraDoDai(value, htmlTagId, message) {
    if (value.length >= 4 && value.length <= 6) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message)
        return false
    }
}
function kiemTraChu(value, htmlTagId, message) {
    var nameReg = /^[a-zA-Z_ÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\ ]+$/;
    var isText = nameReg.test(value)
    if (isText) {
        showMessage(htmlTagId, "")
        return true
    } else {
        showMessage(htmlTagId, message)
        return false
    }
}

function kiemTraEmail(value, htmlTagId, message) {
    var reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmail = reg.test(value);
    if (isEmail) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message);
        return false
    }
}
function kiemTraPass(value, htmlTagId, message) {
    var passReg = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/
    let isPass = passReg.test(value)
    if (isPass) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message)
        return false;
    }
}
function kiemTraNgayThangNam(value, htmlTagId, message) {
    var reg = /^\d{2}\/\d{2}\/\d{4}$/;
    let isDay = reg.test(value)
    if (isDay) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message)
        return false
    }
}
function kiemTraLuong(value, htmlTagId, message) {
    if (value >= 1000000 && value <= 20000000) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message)
        return false
    }
}
function kiemTraGioLam(value, htmlTagId, message) {
    if (value >= 80 && value <= 200) {
        showMessage(htmlTagId, "")
        return true;
    } else {
        showMessage(htmlTagId, message)
        return false
    }
}
function kiemTraChucVu(value, htmlTagId, message) {
    if (value == "1") {
        showMessage(htmlTagId, message)
        return false
    } else {
        showMessage(htmlTagId, "")
        return true;
    }
}